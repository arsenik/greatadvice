package com.yotaphone2.greatadvice.util;

import android.content.Context;

import com.yotaphone2.greatadvice.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arsenik on 11/16/15.
 */
public class SettingsUtil {
    public static Map<String, String> getTagsMap(Context context) {
        HashMap<String, String> tagsMap = new HashMap<String, String>();
        String[] tagsName = context.getResources().getStringArray(R.array.tags);
        String[] tagsVals = context.getResources().getStringArray(R.array.tags_val);
        for (int i = 0; i < tagsName.length; i++) {
            tagsMap.put(tagsName[i], tagsVals[i]);
        }
        return tagsMap;
    }

    public static Map<String, String> getIntervalsMap(Context context) {
        HashMap<String, String> intervalMap = new HashMap<String, String>();
        String[] intervalValue = context.getResources().getStringArray(R.array.interval_vals);
        String[] intervalTitle = context.getResources().getStringArray(R.array.interval);
        for (int i = 0; i < intervalValue.length; i++) {
            intervalMap.put(intervalValue[i], intervalTitle[i]);
        }
        return intervalMap;
    }
}
