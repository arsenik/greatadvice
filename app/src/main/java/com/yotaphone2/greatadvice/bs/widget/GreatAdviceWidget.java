package com.yotaphone2.greatadvice.bs.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.widget.RemoteViews;

import com.yotadevices.sdk.BackscreenLauncherConstants;
import com.yotaphone2.greatadvice.R;
import com.yotaphone2.greatadvice.api.Advice;
import com.yotaphone2.greatadvice.core.GreatAdviceService;
import com.yotaphone2.greatadvice.db.GreatAdviceCache;
import com.yotaphone2.greatadvice.util.SettingsUtil;

/**
 * Created by arsenik on 10/20/15.
 */
public class GreatAdviceWidget extends AppWidgetProvider {
    public static final String STATUS_UPDATE = "statusUpdate";
    public final static String PREFS_PRIVATE_NAME = "greatadvice_private_preferences";

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        GreatAdviceService.receiveNewAdvices(context, null);
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        GreatAdviceService.stopRepeatDownloadAlarm(context);
    }

    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId,
                                          Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        drawWidget(context, appWidgetId, new Bundle());
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        redrawWidgets(context, new Bundle());
    }

    public static void redrawWidgets(Context context, Bundle params) {
        ComponentName componentName = new ComponentName(context, GreatAdviceWidget.class);
        int[] allWidgetIds = AppWidgetManager.getInstance(context).getAppWidgetIds(componentName);
        for (int j = 0; j < allWidgetIds.length; j++) {
            int wId = allWidgetIds[j];
            drawWidget(context, wId, params);
        }
    }

    private static void drawWidget(final Context context, final int wId, Bundle params) {
        Bundle appWidgetOptions = AppWidgetManager.getInstance(context).getAppWidgetOptions(wId);
        boolean isWhite = isWhiteDevice(appWidgetOptions);

        RemoteViews remoteViews = null;
        if (isWhite) {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_advice_view_w);
        } else {
            remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_advice_view_b);
        }

        if (isDemo(appWidgetOptions)) {
            remoteViews.setTextViewText(R.id.advice_tag, "");
            remoteViews.setTextViewText(R.id.advice_content, context.getResources().getString(R.string.demo_advice_text));
            AppWidgetManager.getInstance(context).updateAppWidget(wId, remoteViews);
            return;
        }

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        final boolean censoredAdvices = sharedPref.getBoolean("censoredAdvices", false);
        final boolean showByTagEnabled = sharedPref.getBoolean("showByTagEnabled", false);
        if (showByTagEnabled) {
            String currentShowTag = sharedPref.getString("currentShowTag", null);
            if (currentShowTag != null) {
                remoteViews.setTextViewText(R.id.advice_tag, currentShowTag);
            }
        }

        if (params.getBoolean(STATUS_UPDATE, false)) {
            remoteViews.setTextViewText(R.id.update_btn, context.getString(R.string.update_title));
        }

        Advice advice = null;
        if (showByTagEnabled) {
            String currentShowTag = sharedPref.getString("currentShowTag", null);
            String convTag = SettingsUtil.getTagsMap(context).get(currentShowTag);
            if (convTag != null) {
                int tagNum = Integer.valueOf(convTag);
                advice = GreatAdviceCache.getLastAdviceByTag(context, tagNum);
            }
        } else {
            remoteViews.setTextViewText(R.id.advice_tag, "");
            advice = GreatAdviceCache.getLastAdvice(context, censoredAdvices);
        }

        if (advice != null && !TextUtils.isEmpty(advice.text)) {
            remoteViews.setTextViewText(R.id.advice_content, Html.fromHtml(advice.text));
            remoteViews.setTextViewText(R.id.update_btn, context.getString(R.string.more));
            remoteViews.setOnClickPendingIntent(R.id.update_btn, GreatAdviceService
                    .createUpdateWidgetDataIntent(context));
            AppWidgetManager.getInstance(context).updateAppWidget(wId, remoteViews);
        }
    }

    private static boolean isDemo(Bundle appWidgetOptions) {
        return appWidgetOptions.getBoolean(BackscreenLauncherConstants.OPTION_WIDGET_DEMO_MODE, false);

    }

    private static boolean isWhiteDevice(Bundle appWidgetOptions) {
        int theme = appWidgetOptions.getInt(BackscreenLauncherConstants.OPTION_WIDGET_THEME, -1);
        return theme == BackscreenLauncherConstants.WIDGET_THEME_WHITE;
    }
}
