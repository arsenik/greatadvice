package com.yotaphone2.greatadvice.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by arsenik on 10/20/15.
 */
public class GreatAdviceDbProvider extends ContentProvider {
    private DatabaseHelper mOpenHelper;
    private static UriMatcher sUriMatcher;
    private final static int URI_ADVICES = 1;
    public static final String AUTHORITY = "com.yotaphone2.greatadvice";

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, AdvicesTable.TABLE_NAME, URI_ADVICES);
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mOpenHelper = new DatabaseHelper(context);
        return true;
    }

    public int getMatchCode(Uri uri) {
        final int match = sUriMatcher.match(uri);
        return match;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        int match = getMatchCode(uri);
        switch (match) {
            case URI_ADVICES:
                Cursor cursor = db.query(AdvicesTable.TABLE_NAME, projection, selection,
                        selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(getContext().getContentResolver(),
                        AdvicesTable.CONTENT_URI);
                return cursor;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowID = db.insert(AdvicesTable.TABLE_NAME, null, values);
        Uri resultUri = ContentUris.withAppendedId(AdvicesTable.CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        int count = db.delete(AdvicesTable.TABLE_NAME, selection, selectionArgs);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
