package com.yotaphone2.greatadvice.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.yotaphone2.greatadvice.api.Advice;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arsenik on 10/20/15.
 */
public class GreatAdviceCache {
    public static void savePosts(Context context, List<Advice> advices) {
        ContentValues[] values = new ContentValues[advices.size()];
        int i = 0;
        for (Advice advice : advices) {
            ContentValues contentValues = advice.getValues();
            values[i] = contentValues;
            i++;
        }

        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.bulkInsert(AdvicesTable.CONTENT_URI, values);
    }

    public static List<Advice> getPosts(Context context) {
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        List<Advice> advices = new ArrayList<Advice>();

        try {
            cursor = contentResolver.query(AdvicesTable.CONTENT_URI, null,
                    null, null, Advice.ID + " DESC");
            while (cursor != null && cursor.moveToNext()) {
                advices.add(new Advice(cursor));
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return advices;
    }

    public static void clearPosts(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        contentResolver.delete(AdvicesTable.CONTENT_URI, null, null);
    }

    public static List<Advice> getLastAdvices(Context context, int count) {
        List<Advice> posts = new ArrayList<Advice>();
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        try {
            cursor = contentResolver.query(AdvicesTable.CONTENT_URI, null,
                    null, null, Advice.ID + " DESC LIMIT " + count);
            while (cursor != null && cursor.moveToNext()) {
                posts.add(new Advice(cursor));
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return posts;
    }

    public static Advice getLastAdviceByTag(Context context, int tagNum) {
        Advice advice = null;
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        try {
            String where = Advice.TAG_NUM + " = " + tagNum;
            cursor = contentResolver.query(AdvicesTable.CONTENT_URI, null,
                    where, null, "_id DESC LIMIT 1");
            if (cursor != null && cursor.moveToFirst()) {
                advice = new Advice(cursor);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return advice;
    }

    public static Advice getLastAdvice(Context context, boolean censored) {
        Advice advice = null;
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        try {
            String selection = Advice.CENSORED + " = ?";
            String[] selectionArgs = new String[]{ String.valueOf(censored ? 1 : 0) };
            cursor = contentResolver.query(AdvicesTable.CONTENT_URI, null,
                    selection, selectionArgs, "_id DESC LIMIT 1");
            if (cursor != null && cursor.moveToFirst()) {
                advice = new Advice(cursor);
            }

            return advice;
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }
}
