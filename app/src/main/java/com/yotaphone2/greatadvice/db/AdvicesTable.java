package com.yotaphone2.greatadvice.db;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import com.yotaphone2.greatadvice.api.Advice;

/**
 * Created by arsenik on 10/13/15.
 */
public class AdvicesTable {
    public static final Uri CONTENT_URI = Uri.parse("content://"
            + GreatAdviceDbProvider.AUTHORITY + "/" + AdvicesTable.TABLE_NAME);

    public static final String TABLE_NAME = "advices";

    private static final String DROP_QUERY = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String CREATE_QUERY = "create table "
            + TABLE_NAME + " ("
            + BaseColumns._ID + " integer primary key autoincrement, "
            + Advice.ID + " integer, "
            + Advice.CENSORED + " integer default 0, "
            + Advice.TAG_NUM + " integer default -1, "
            + Advice.TEXT + " text);";

    public static final void create(SQLiteDatabase db) {
        db.execSQL(CREATE_QUERY);
    }

    public static final void drop(SQLiteDatabase db) {
        db.execSQL(DROP_QUERY);
    }
}
