package com.yotaphone2.greatadvice.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yotaphone2.greatadvice.api.Settings;

import java.lang.reflect.Modifier;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class ServiceGenerator {

    // No need to instantiate this class.
    private ServiceGenerator() {
    }

    public static <S> S createService(Class<S> serviceClass) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PRIVATE).create();
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(Settings.API_URL)
                .setConverter(new GsonConverter(gson));

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }
}