package com.yotaphone2.greatadvice.core;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.yotaphone2.greatadvice.R;
import com.yotaphone2.greatadvice.api.Advice;
import com.yotaphone2.greatadvice.api.GreatAdviceClient;
import com.yotaphone2.greatadvice.bs.widget.GreatAdviceWidget;
import com.yotaphone2.greatadvice.db.GreatAdviceCache;
import com.yotaphone2.greatadvice.util.ConnectUtil;
import com.yotaphone2.greatadvice.util.SettingsUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by arsenik on 10/20/15.
 */
public class GreatAdviceService extends IntentService {

    public static final String GREAT_ADVICE_SERVICE = "GreatAdviceService";
    public static final String MESSENGER = "com.yotaphone2.greatadvice.Messenger";
    public static final String ACTION_RECEIVE_NEW_ADVICES = "com.yotaphone2.greatadvic.ACTION_RECEIVE_NEW_ADVICES";

    private static final long UPDATE_INTERVAL = 1 * 60 * 1000; // 1 min

    public static final String LOCK_WIFI_NAME = "WakefulAdviceLock";
    private WifiManager.WifiLock wifiLock;

    public GreatAdviceService() {
        super(GREAT_ADVICE_SERVICE);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        WifiManager wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, LOCK_WIFI_NAME);
        wifiLock.setReferenceCounted(true);
    }

    @Override
    public void onStart(Intent intent, final int startId) {
        wifiLock.acquire();
        super.onStart(intent, startId);
    }

    public static void stopRepeatDownloadAlarm(Context context) {
        Intent intent = new Intent(context, GreatAdviceService.class);
        intent.setAction(ACTION_RECEIVE_NEW_ADVICES);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(alarmIntent);
    }

    public static void startRepeatDownloadAlarm(Context context) {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int updateInterval = Integer.valueOf(sharedPref.getString("updateInterval", String.valueOf(0)));
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, GreatAdviceService.class);
        intent.setAction(ACTION_RECEIVE_NEW_ADVICES);
        PendingIntent alarmIntent = PendingIntent.getService(context, 0, intent, 0);

        if (updateInterval > 0) {
            int type = AlarmManager.RTC_WAKEUP;
            long period = updateInterval * 1000 * 60;
            Log.d("updateIntervalPeriod", "period: " + period);
            Calendar c = Calendar.getInstance();
            am.set(type, c.getTimeInMillis() + period, alarmIntent);
        } else {
            am.cancel(alarmIntent);
        }
    }

    public static void receiveNewAdvices(Context context) {
        receiveNewAdvices(context, null);
    }

    public static void receiveNewAdvices(Context context, Messenger messenger) {
        Intent intent = new Intent(context, GreatAdviceService.class);
        intent.setAction(ACTION_RECEIVE_NEW_ADVICES);
        if (messenger != null) {
            intent.putExtra(MESSENGER, messenger);
        }
        context.startService(intent);
    }

    public static PendingIntent createUpdateWidgetDataIntent(Context ctx) {
        Intent intent = new Intent(ctx, GreatAdviceService.class);
        intent.setAction(ACTION_RECEIVE_NEW_ADVICES);
        PendingIntent pendingIntent = PendingIntent.getService(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
        }

        WakefulBroadcastReceiver.completeWakefulIntent(intent);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        String action = intent.getAction();
        if (action != null) {
            if (action.equals(ACTION_RECEIVE_NEW_ADVICES)) {
                boolean connected = ConnectUtil.isConnected(this);
                if (!connected) {
                    sharedPref.edit().putInt("attempts", 3).apply();
                    return;
                }

                GreatAdviceClient client = ServiceGenerator.createService(GreatAdviceClient.class);

                Bundle params = new Bundle();
                params.putBoolean(GreatAdviceWidget.STATUS_UPDATE, true);
                GreatAdviceWidget.redrawWidgets(this, params);

                final boolean censoredAdvices = sharedPref.getBoolean("censoredAdvices", false);
                final boolean showByTagEnabled = sharedPref.getBoolean("showByTagEnabled", false);

                Callback<Advice> getAdviceCallback = new Callback<Advice>() {
                    @Override
                    public void success(Advice advice, Response response) {
                        if (showByTagEnabled) {
                            String currentShowTag = sharedPref.getString("currentShowTag", String.valueOf(1));
                            String convTag = SettingsUtil.getTagsMap(GreatAdviceService.this).get(currentShowTag);
                            if (convTag != null) {
                                int tagNum = Integer.valueOf(convTag);
                                advice.setTagNum(tagNum);
                            }
                        }

                        advice.setCensored(censoredAdvices);

                        final List<Advice> list = new ArrayList<Advice>();
                        list.add(advice);

                        GreatAdviceCache.savePosts(getApplicationContext(), list);

                        SharedPreferences prefs = getSharedPreferences(GreatAdviceWidget.PREFS_PRIVATE_NAME,
                                Context.MODE_PRIVATE);
                        prefs.edit().putInt("attempts", 0).apply();

                        GreatAdviceWidget.redrawWidgets(getApplicationContext(), new Bundle());

                        Messenger messenger = intent.getParcelableExtra(MESSENGER);
                        if (messenger != null) {
                            try {
                                messenger.send(Message.obtain());
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d("postsReceive", "failure");
                    }
                };

                if (showByTagEnabled) {
                    String currentShowTag = sharedPref.getString("currentShowTag", getResources().getStringArray(R.array.tags)[0]);
                    client.randomByTag(currentShowTag, getAdviceCallback);
                } else {
                    if (censoredAdvices) {
                        client.randomCensoredAdvice(getAdviceCallback);
                    } else {
                        client.randomAdvice(getAdviceCallback);
                    }
                }


                startRepeatDownloadAlarm(this);
            }
        }

    }
}
