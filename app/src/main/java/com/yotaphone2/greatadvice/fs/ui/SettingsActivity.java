package com.yotaphone2.greatadvice.fs.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.yotaphone2.greatadvice.R;
import com.yotaphone2.greatadvice.core.GreatAdviceService;
import com.yotaphone2.greatadvice.util.SettingsUtil;

import java.util.Map;

/**
 * Created by arsenik on 11/12/15.
 */
public class SettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public static class SettingsFragment extends PreferenceFragment {
        private CheckBoxPreference mCensoredCheckBox;
        private ListPreference mTagPref;
        private ListPreference mUpdateIntervalPref;

        private SharedPreferences.OnSharedPreferenceChangeListener mPrefChangeListener = new SharedPreferences
                .OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPrefs,
                                                  String key) {
                if (key.equals("updateInterval")) {
                    GreatAdviceService.stopRepeatDownloadAlarm(getActivity());
                    GreatAdviceService.startRepeatDownloadAlarm(getActivity());
                }

                SettingsFragment settingFragment = (SettingsFragment) getFragmentManager().findFragmentById(android.R
                        .id.content);

                String updateInterval = sharedPrefs.getString("updateInterval", String.valueOf(0));
                Log.d("updateInterval", "updateInterval: " + updateInterval);

                settingFragment.updateIntervalUI(sharedPrefs);
                settingFragment.updateCurrentTagUI(sharedPrefs);
                settingFragment.updateCensoredUI(sharedPrefs);
                GreatAdviceService.receiveNewAdvices(getActivity());
            }
        };

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.user_settings);
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
            mCensoredCheckBox = (CheckBoxPreference) findPreference("censoredAdvices");
            mTagPref = (ListPreference) findPreference("currentShowTag");
            mUpdateIntervalPref = (ListPreference) findPreference("updateInterval");

            sharedPref.registerOnSharedPreferenceChangeListener(mPrefChangeListener);

            getChildFragmentManager().executePendingTransactions();

            updateIntervalUI(sharedPref);
            updateCurrentTagUI(sharedPref);
            updateCensoredUI(sharedPref);
        }

        @Override
        public void onPause() {
            super.onPause();
            PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(mPrefChangeListener);
        }

        public void updateCensoredUI(SharedPreferences sharedPref) {
            if (sharedPref.getBoolean("showByTagEnabled", false)) {
                mCensoredCheckBox.setChecked(false);
                mCensoredCheckBox.setEnabled(false);
            } else {
                mCensoredCheckBox.setEnabled(true);
                mCensoredCheckBox.setChecked(sharedPref.getBoolean("censoredAdvices", false));
            }
        }

        public void updateCurrentTagUI(SharedPreferences sharedPref) {
            String[] tagsName = getResources().getStringArray(R.array.tags);
            String tagValue = sharedPref.getString("currentShowTag", tagsName[0]);
            mTagPref.setSummary(tagValue);
        }

        public void updateIntervalUI(SharedPreferences sharedPref) {
            String intervalValue = sharedPref.getString("updateInterval", String.valueOf(0));
            Map<String, String> intervals = SettingsUtil.getIntervalsMap(getActivity());
            mUpdateIntervalPref.setSummary(intervals.get(intervalValue));
        }
    }

}
