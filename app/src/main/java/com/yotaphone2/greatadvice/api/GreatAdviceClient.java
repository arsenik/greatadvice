package com.yotaphone2.greatadvice.api;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by arsenik on 7/15/15.
 */
public interface GreatAdviceClient {
    @GET("/random")
    void randomAdvice(
            Callback<Advice> callback
    );

    @GET("/random_by_tag/{tag}")
    void randomByTag(
            @Path("tag") String tag,
            Callback<Advice> callback
    );

    @GET("/random/censored")
    void randomCensoredAdvice(
            Callback<Advice> callback
    );

}
