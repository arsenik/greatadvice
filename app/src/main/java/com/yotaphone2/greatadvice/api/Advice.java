package com.yotaphone2.greatadvice.api;

import android.content.ContentValues;
import android.database.Cursor;

public class Advice {
    public static final String ID = "id";
    public static final String TEXT = "text";
    public static final String CENSORED = "censored";
    public static final String TAG_NUM = "tagNum";

    public long id;
    public String text;

    private int tagNum;
    private boolean censored = false;

    public ContentValues getValues() {
        ContentValues cv = new ContentValues();
        cv.put(Advice.ID, id);
        cv.put(Advice.TEXT, text);
        cv.put(Advice.TAG_NUM, tagNum);
        cv.put(Advice.CENSORED, censored ? 1 : 0);
        return cv;
    }

    public Advice(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndex(Advice.ID));
        text = cursor.getString(cursor.getColumnIndex(Advice.TEXT));
        censored = cursor.getInt(cursor.getColumnIndex(Advice.CENSORED)) == 1;
        tagNum = cursor.getInt(cursor.getColumnIndex(Advice.TAG_NUM));
    }

    public boolean getCensored() {
        return this.censored;
    }

    public int getTagNum() {
        return this.tagNum;
    }

    public void setTagNum(int tagNum) {
        this.tagNum = tagNum;
    }

    public void setCensored(boolean censored) {
        this.censored = censored;
    }
}